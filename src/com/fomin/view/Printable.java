package com.fomin.view;

public interface Printable {
    void print();
}
