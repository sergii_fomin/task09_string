package com.fomin.view;

import com.fomin.controller.Controller;

import java.util.*;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    private Controller controller;

    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("10", bundle.getString("10"));
        menu.put("Q", bundle.getString("Q"));
    }

    public MyView() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", controller::concatenatesParams);
        methodsMenu.put("2", controller::checkSentence);
        methodsMenu.put("3", controller::splitString);
        methodsMenu.put("4", controller::replaceVowels);
        methodsMenu.put("8", this::internationalizeMenuEnglish);
        methodsMenu.put("9", this::internationalizeMenuJapanese);
        methodsMenu.put("10", this::internationalizeMenuGreek);
        methodsMenu.put("11", this::internationalizeMenuCzech);
        methodsMenu.put("12", this::internationalizeMenuGerman);
        methodsMenu.put("Q", () -> System.exit(0));
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en", "US");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuJapanese() {
        locale = new Locale("ja", "JA");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuGreek() {
        locale = new Locale("el", "EL");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuCzech() {
        locale = new Locale("cs", "CS");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuGerman() {
        locale = new Locale("de", "DE");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
        show();
    }

    private void outputMenu() {
        System.out.println("\n MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
