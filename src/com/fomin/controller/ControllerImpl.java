package com.fomin.controller;

import com.fomin.model.ReadFile;

public class ControllerImpl implements Controller {

    private ReadFile fileService;

    private String path = "/home/nomorethrow/IdeaProjects/task09_string/src/main/resources/";

    private String filename = "text.txt";

    public ControllerImpl(ReadFile fileService) {
        this.fileService = fileService;
    }

    @Override
    public void concatenatesParams() {
    }

    @Override
    public void checkSentence() {
    }

    @Override
    public void splitString() {
    }

    @Override
    public void replaceVowels() {
    }
}
