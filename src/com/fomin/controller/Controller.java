package com.fomin.controller;

public interface Controller {

    void concatenatesParams();

    void checkSentence();

    void splitString();

    void replaceVowels();

}
