package com.fomin.utils;

import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringUtils {
    public static String concatAllParameters(List<? extends CharSequence> params) {
        StringBuilder result = new StringBuilder();
        for (CharSequence param : params) {
            String p = param.toString().replaceAll("\\s+", "");
            result.append(p);
        }
        return result.toString();
    }

    public static boolean checkSentence(String sentence) {
        Pattern pattern = Pattern.compile("\\b([A-Z][a-z]*)\\b(.$)");
        Matcher matcher = pattern.matcher(sentence);
        return matcher.matches();
    }

    public static String splitString(String text) {
        String[] splitted = text.split("\\bthe|you\\b");
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : splitted) {
            stringBuilder.append(s);
        }
        return stringBuilder.toString().trim();
    }

    public static String replaceVowels(String word) {
        return word.replaceAll("[aeoui]", "_");
    }

    public static List<String> sortTextByWordCount(List<String> text) {
        text.sort(new SentenceComparator());
        return text;
    }

    public static String findUniqueWordInFirstSentence(List<String> text) {
        String firstSentence = text.get(0);
        String[] firstSentenceWords = firstSentence.split(" ");

        String uniqueWord = "";
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String firstSentenceWord : firstSentenceWords) {
                for (String word : words) {
                    if (!word.equals(firstSentenceWord)) {
                        uniqueWord = firstSentenceWord;
                    }
                }
            }
        }
        return uniqueWord;
    }

    public static List<String> questionsUniqueWords(List<String> text, int length) {
        List<String> questions = text
                .stream()
                .filter(s -> s.endsWith("?"))
                .collect(Collectors.toList());
        List<String> uniqueQuestionsWords = questions
                .stream()
                .distinct()
                .collect(Collectors.toList());
        return uniqueQuestionsWords
                .stream()
                .filter(q -> q.length() == length)
                .collect(Collectors.toList());
    }

    static class SentenceComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            String[] word1 = o1.split(" ");
            String[] word2 = o2.split(" ");
            if (word1.length > word2.length) {
                return 1;
            } else if (word1.length == word2.length) {
                return 0;
            }
            return -1;
        }
    }
}
