package com.fomin.model;

import java.io.IOException;
import java.util.List;

public interface ReadFile {
    List<String> readFromFile(String path) throws IOException;
}
