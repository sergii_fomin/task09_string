package com.fomin.model;

import com.fomin.controller.Controller;
import com.fomin.utils.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ReadFileImpl implements ReadFile {
    private ReadFile readFile;
    private String path = "D/Epam/Tasks/My/task09_string/src/resources/";
    private String filename = "text.txt";

    public ReadFileImpl(ReadFile readFile) {
        this.readFile = readFile;
    }

    @Override
    public List<String> readFromFile(String filename) throws IOException {
        File file = new File(filename);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        List<String> lines = new ArrayList<>();
        for (String line; (line = bufferedReader.readLine()) != null;) {
            lines.add(line);
        }
        List<String> result = new ArrayList<>();
        for (String line : lines) {
            String[] sentences = line.split("[.!?]");
            result.addAll(Arrays.asList(sentences));
        }
        return result;
    }
}
