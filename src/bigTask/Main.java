package bigTask;

import bigTask.controller.Controller;
import bigTask.controller.impl.ControllerImpl;
import bigTask.services.ReadFile;
import bigTask.services.impl.ReadFileImpl;
import bigTask.view.MyView;

public class Main {
    public static void main(String[] args) {
        ReadFile readFile = (ReadFile) new ReadFileImpl();
        Controller controller = new ControllerImpl(readFile);
        new MyView(controller);
    }
}
