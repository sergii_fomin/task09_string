package bigTask.view;

import bigTask.controller.Controller;

import java.util.*;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("10", bundle.getString("10"));
        menu.put("11", bundle.getString("11"));
        menu.put("12", bundle.getString("12"));
        menu.put("13", bundle.getString("13"));
        menu.put("Q", bundle.getString("Q"));
    }

    public MyView(Controller controller) {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this.controller::alphabeticalSort);
        methodsMenu.put("2", this.controller::findUniqueWordInFirstSentence);
        methodsMenu.put("3", this.controller::sortTextByWordCount);
        methodsMenu.put("4", this.controller::sortByVowelsPercentage);
        methodsMenu.put("5", this.controller::sortByFirstConsonantLetter);
        methodsMenu.put("6", this.controller::sortByLetterGrowth);
        methodsMenu.put("7", this.controller::sortByWordOccurrence);
        methodsMenu.put("8", this.controller::sortByLetterOccurrenceAlphabetical);
        methodsMenu.put("9", this.controller::removeWordsWithSpecificLength);
        methodsMenu.put("10", this.controller::removeLetters);
        methodsMenu.put("11", this.controller::replaceString);
        methodsMenu.put("12", this.controller::replaceWithSubstring);
        methodsMenu.put("13", this.controller::questionsUniqueWords);
        methodsMenu.put("Q", () -> System.exit(0));
    }

    private void outputMenu() {
        System.out.println("\n MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
