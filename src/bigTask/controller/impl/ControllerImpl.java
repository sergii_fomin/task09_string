package bigTask.controller.impl;

import bigTask.controller.Controller;
import bigTask.services.ReadFile;
import bigTask.utils.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ControllerImpl implements Controller {

    private ReadFile readFile;

    private String path = "resources/";

    private String filename = "text.txt";

    public ControllerImpl(ReadFile readFile) {
        this.readFile = readFile;
    }

    @Override
    public void replaceString() {
        try {
            StringUtils.replaceWords(readFile.readFromFile(path + filename))
                    .forEach(s -> System.out.print(s + " "));
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alphabeticalSort() {
        try {
            StringUtils.sortAlphabetical(readFile.readFromFile(path + filename))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortByVowelsPercentage() {
        try {
            StringUtils.sortByVowelsPercentage(readFile.readFromFile(path + filename))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortByFirstConsonantLetter() {
        try {
            StringUtils.sortByFirstConsonantLetter(readFile.readFromFile(path + filename))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortByLetterGrowth() {
        Scanner scanner = new Scanner(System.in);
        char letter = scanner.next().charAt(0);
        try {
            StringUtils.sortByLetterGrowth(readFile.readFromFile(path + filename), letter)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortByWordOccurrence() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        List<String> words = Arrays.asList(input.split(" "));
        try {
            StringUtils.sortByWordOccurrence(readFile.readFromFile(path + filename), words)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeWordsWithSpecificLength() {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        try {
            StringUtils.removeWordWithSpecificLength(readFile.readFromFile(path + filename), length)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortByLetterOccurrenceAlphabetical() {
        Scanner scanner = new Scanner(System.in);
        char letter = scanner.nextLine().charAt(0);
        try {
            StringUtils.sortWordsByLetterOccurrence(readFile.readFromFile(path + filename), letter)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeLetters() {
        try {
            StringUtils.removeLetters(readFile.readFromFile(path + filename))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void replaceWithSubstring() {
        Scanner scanner = new Scanner(System.in);
        int wordLength = scanner.nextInt();
        scanner.nextLine();
        String substring = scanner.nextLine();
        try {
            StringUtils.replaceWithSubstring(readFile.readFromFile(path + filename), wordLength, substring)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortTextByWordCount() {
        try {
            List<String> orderedText = StringUtils.sortTextByWordCount(readFile.readFromFile(path + filename));
            orderedText.forEach(s -> System.out.print(s + " "));
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void findUniqueWordInFirstSentence() {
        try {
            List<String> text = readFile.readFromFile(path + filename);
            System.out.println(StringUtils.findUniqueWordInFirstSentence(text));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void questionsUniqueWords() {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        try {
            StringUtils.questionsUniqueWords(readFile.readFromFile(path + filename), length)
                    .forEach(w -> System.out.print(w + " "));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
