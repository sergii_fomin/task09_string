package bigTask.controller;

public interface Controller {
    void replaceString();
    void alphabeticalSort();
    void sortByVowelsPercentage();
    void sortByFirstConsonantLetter();
    void sortByLetterGrowth();
    void sortByWordOccurrence();
    void removeWordsWithSpecificLength();
    void sortByLetterOccurrenceAlphabetical();
    void removeLetters();
    void replaceWithSubstring();
    void sortTextByWordCount();
    void findUniqueWordInFirstSentence();
    void questionsUniqueWords();
}
